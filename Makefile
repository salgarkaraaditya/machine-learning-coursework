# Define the target PDF file
OUTPUT_PDF = notes.pdf

# Find all .tex files in subdirectories
TEX_FILES := $(shell find . -type f -name '*.tex')


all:
	mkdir -p output
	for file in $(TEX_FILES); do \
		echo "Compiling $$file"; \
		latexmk -pdf -outdir=output $$file 2>/dev/null >/dev/null; \
	done

	rm -rf */*.aux */*.log */*.out */*.toc */*.fls */*.fdb_latexmk */*.synctex.gz
	pdftk output/*.pdf output notes.pdf 
	rm -rf Lec*/*.pdf
	rm -rf output



clean:
	rm -rf */*.aux */*.log */*.out */*.toc */*.fls */*.fdb_latexmk */*.synctex.gz

.PHONY: all clean
